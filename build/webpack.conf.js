var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        main: '../demo/main',
        vendor: ['vue', 'vue-router']
    },
    output: {
        path: path.join(__dirname, '../dist'),
        publicPath: '/dist/',
        filename: '[name].js',
        chunkFilename: '[name].chunk.js'
    },
    // 加载器
    module: {
        loaders: [{
            test: /\.vue$/,
            loader: 'vue'
        }, {
            test: /\.js$/,
            loader: 'babel',
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            loader: 'style!css!autoprefixer'
        }, {
            test: /\.less$/,
            loader: 'style!css!less'
        }, {
            test: /\.scss$/,
            loader: 'style!css!sass?sourceMap'
        }, {
            test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
            loader: 'url-loader?limit=8192'
        }, {
            test: /\.(html|tpl)$/,
            loader: 'html-loader'
        }]
    },
    vue: {
        loaders: {
            css: ExtractTextPlugin.extract(
                "style-loader",
                "css-loader?sourceMap", {
                    publicPath: "../dist/"
                }
            ),
            less: ExtractTextPlugin.extract(
                'vue-style-loader',
                'css-loader!less-loader'
            ),
            js: 'babel'
        }
    },
    // 转es5
    babel: {
        presets: ['es2015'],
        plugins: ['transform-runtime']
    },
    resolve: {
        // require时省略的扩展名，如：require('module') 不需要module.js
        extensions: ['', '.js', '.vue']
    },
    plugins: [
        new ExtractTextPlugin("[name].css", {
            allChunks: true,
            resolve: ['modules']
        }), // 提取CSS
        new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js'), // 提取第三方库
        new HtmlWebpackPlugin({ // 构建html文件
            filename: '../index.html',
            template: '../demo/template/index.html',
            inject: 'body'
        })
    ]
}